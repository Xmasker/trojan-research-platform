#encoding=utf-8
import sys
import json
import time

from aliyunsdkcore.acs_exception.exceptions import ServerException, ClientException
from aliyunsdkvpc.request.v20160428 import CreateVpcRequest
from aliyunsdkvpc.request.v20160428 import CreateVSwitchRequest
from aliyunsdkvpc.request.v20160428 import DeleteVSwitchRequest
from aliyunsdkvpc.request.v20160428 import DeleteVpcRequest
from aliyunsdkvpc.request.v20160428 import DescribeVSwitchAttributesRequest
from aliyunsdkvpc.request.v20160428 import DescribeVpcAttributeRequest
from aliyunsdkcore.client import AcsClient

class VpcQuickStart(object):
    def __init__(self, client):
        self.client = client
        self.TIME_DEFAULT_OUT = 15
        self.DEFAULT_TIME = 1

    def check_status(self,time_default_out, default_time, func, check_status, id):
        for i in range(time_default_out):
            time.sleep(default_time)
            status = func(id)
            if status == check_status:
                return True
        return False

    def create_vpc(self):
        try:
            request = CreateVpcRequest.CreateVpcRequest()
            response = self.client.do_action_with_exception(request)
            response_json = json.loads(response)
            # 判断VPC状态是否可用
            if self.check_status(self.TIME_DEFAULT_OUT, self.DEFAULT_TIME,
                                        self.describe_vpc_status,
                                        "Available", response_json['VpcId']):
                return response_json
        except ServerException as e:
            print(e)
        except ClientException as e:
            print(e)

    def delete_vpc(self, params):
        try:
            request = DeleteVpcRequest.DeleteVpcRequest()
            # 要删除的VPC的ID
            request.set_VpcId(params['vpc_id'])
            response = self.client.do_action_with_exception(request)
            response_json = json.loads(response)
            return response_json
        except ServerException as e:
            print(e)
        except ClientException as e:
            print(e)

    def describe_vpc_attribute(self, vpc_id):
        try:
            request = DescribeVpcAttributeRequest.DescribeVpcAttributeRequest()
            # 要查询的VPC的ID
            request.set_VpcId(vpc_id)
            response = self.client.do_action_with_exception(request)
            response_json = json.loads(response)
            return response_json
        except ServerException as e:
            print(e)
        except ClientException as e:
            print(e)

    def describe_vpc_status(self, vpc_id):
        response = self.describe_vpc_attribute(vpc_id)
        return response["Status"]

    def create_vswitch(self, params):
        try:
            request = CreateVSwitchRequest.CreateVSwitchRequest()
            # 交换机所属区的ID，您可以通过调用DescribeZones接口获取地域ID
            request.set_ZoneId(params['zone_id'])
            # 交换机所属的VPC的ID
            request.set_VpcId(params['vpc_id'])
            # 交换机的网段
            request.set_CidrBlock(params['cidr_block'])
            response = self.client.do_action_with_exception(request)
            response_json = json.loads(response)
            # 判断交换机状态是否可用
            if self.check_status(self.TIME_DEFAULT_OUT, self.DEFAULT_TIME,
                                        self.describe_vswitch_status,
                                        "Available", response_json['VSwitchId']):
                return response_json
        except ServerException as e:
            print(e)
        except ClientException as e:
            print(e)

    def describe_vswitch_attribute(self, vswitch_id):
        try:
            request = DescribeVSwitchAttributesRequest.DescribeVSwitchAttributesRequest()
            request.set_VSwitchId(vswitch_id)
            response = self.client.do_action_with_exception(request)
            response_json = json.loads(response)
            return response_json
        except ServerException as e:
            print(e)
        except ClientException as e:
            print(e)

    def describe_vswitch_status(self, vswitch_id):
        response = self.describe_vswitch_attribute(vswitch_id)
        return response["Status"]

    def delete_vswitch(self, params):
        try:
            request = DeleteVSwitchRequest.DeleteVSwitchRequest()
            # 要删除的交换机的ID
            request.set_VSwitchId(params['vswitch_id'])
            response = self.client.do_action_with_exception(request)
            response_json = json.loads(response)
            # 判断交换机是否被删除成功
            if self.check_status(self.TIME_DEFAULT_OUT, self.DEFAULT_TIME * 5,
                                        self.describe_vswitch_status,
                                        '', params['vswitch_id']):
                return response_json
        except ServerException as e:
            print(e)
        except ClientException as e:
            print(e)

if __name__ == "__main__":
    client = AcsClient('accessKeyId','accessSecret','cn-shanghai',timeout = 35)
    vpc_quick_start = VpcQuickStart(client)

    params = {}
    params['zone_id'] = "cn-zhangjiakou-b"
    params['cidr_block'] = "172.16.0.0/16"

    # 创建vpc
    vpc_json = vpc_quick_start.create_vpc()
    print("---------------------------create_vpc---------------------------")
    print(vpc_json)

    # 创建vSwitch
    params['vpc_id'] = vpc_json['VpcId']
    vswitch_json = vpc_quick_start.create_vswitch(params)
    print("---------------------------create_vswitch---------------------------")
    print(vswitch_json)

    # 删除vSwitch
    params['vswitch_id'] = vswitch_json['VSwitchId']
    vswitch_json = vpc_quick_start.delete_vswitch(params)
    print("---------------------------delete_vswitch---------------------------")
    print(vswitch_json)

    # 删除vpc
    vpc_json = vpc_quick_start.delete_vpc(params)
    print("---------------------------delete_vpc---------------------------")
    print(vpc_json)