#!/usr/bin/env python
# coding=utf-8

from aliyunsdkcore import client
from aliyunsdkecs.request.v20140526 import DescribeSecurityGroupAttributeRequest
from aliyunsdkecs.request.v20140526 import DescribeSecurityGroupsRequest
from aliyunsdkecs.request.v20140526 import RevokeSecurityGroupRequest
from aliyunsdkecs.request.v20140526 import AuthorizeSecurityGroupRequest
import json

# 查询安全组规则
def describeSecurityGroupAttributeRequest():
    request = DescribeSecurityGroupAttributeRequest.DescribeSecurityGroupAttributeRequest()
    request.set_SecurityGroupId('sg-9xxxx')
    request.set_accept_format('json')
    return request

# 查询安全组名称
def describeSecurityGroupsRequest():
    request = DescribeSecurityGroupsRequest.DescribeSecurityGroupsRequest()
    request.set_accept_format('json')
    return request

#撤销安全组规则
def revokeSecurityGroupRequest():
    request = RevokeSecurityGroupRequest.RevokeSecurityGroupRequest()
    request.set_SecurityGroupId('sg-9xxxx')
    request.set_IpProtocol('all')
    request.set_PortRange('-1/-1')
    request.set_SourceCidrIp('0.0.0.0/0')
    request.set_Policy('accept')
    request.set_accept_format('json')
    return request

#授权安全组规则
def authorizeSecurityGroupRequest():
    request = AuthorizeSecurityGroupRequest.AuthorizeSecurityGroupRequest()
    request.set_SecurityGroupId('sg-9xxxx')
    request.set_IpProtocol('tcp')
    request.set_PortRange('3389/3389')
    request.set_SourceCidrIp('0.0.0.0/0')
    request.set_Priority('100')
    request.set_Policy('accept')
    request.set_accept_format('json')
    return request

if __name__ == '__main__':
    clt = client.AcsClient('AccessKeyId','AccessKeySecret','cn-beijing')
    #request = revokeSecurityGroupRequest()
    #request = authorizeSecurityGroupRequest()
    #request = describeSecurityGroupAttributeRequest()
    request = describeSecurityGroupsRequest()
    result = clt.do_action(request)
    print(result)
