#!/usr/bin/env python
#coding=utf-8
import json
from aliyunsdkcore.client import AcsClient
from aliyunsdkcore.acs_exception.exceptions import ClientException
from aliyunsdkcore.acs_exception.exceptions import ServerException
from aliyunsdkvpc.request.v20160428 import CreateVpcRequest
from aliyunsdkvpc.request.v20160428 import DescribeVpcAttributeRequest
# 创建 AcsClient 实例
client = AcsClient(
   "<your-access-key-id>",
   "<your-access-key-secret>",
   "<your-region-id>"
)
# 创建 VPC
request = CreateVpcRequest.CreateVpcRequest()
# 为VPC网络设置一个名字
request.set_VpcName('youvpcname')
# 配置VPC的网段CIDR地址
request.set_CidrBlock('192.168.0.0/16')
response = client.do_action_with_exception(request)
vpc_id = json.loads(response)['VpcId']
print ("VPC ID is"+str(vpc_id))
# 获取并打印 VPC 的属性信息
request = DescribeVpcAttributeRequest.DescribeVpcAttributeRequest()
request.set_VpcId(vpc_id)
response = client.do_action_with_exception(request)
print(response)